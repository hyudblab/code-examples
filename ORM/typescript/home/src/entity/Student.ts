import { Entity, PrimaryColumn, Column } from 'typeorm'


@Entity("student")
export class Student {
    @PrimaryColumn()
    sid: string

    @Column()
    name: string

    @Column()
    login: string

    @Column("int")
    age: number

    @Column("float")
    gpa: number

    @Column("date")
    firstyear: string
}
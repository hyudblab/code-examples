import 'reflect-metadata'
import { createConnection } from 'typeorm'
import { Student } from './entity/Student'

async function main() {
  const connection = await createConnection('dbcenter')
  console.log(connection)

  let studentRepo = connection.getRepository(Student)
  let eun = await studentRepo.findOne({ name: "원은영" })
  console.log("mydb", eun)
  eun.gpa = 3.3
  await studentRepo.save(eun)
  console.log("updated: ", eun)
}

main()
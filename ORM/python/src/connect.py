import json
import psycopg2 as pg
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
import traceback


class Connector:
    def __init__(self, db_name, db_type):
        if db_type == 'pg':
            try:
                self.conn = pg.connect(open_db_props(db_name))
                self.db_type = 'pg'
            except Exception:
                traceback.print_exc
        else:
            engine = create_engine(open_db_props(db_name))
            self.conn = Session(engine)
            self.db_type = db_type

    def close(self):
        self.conn.close()


# db_props in 'db.properties' (json) file
# {
#     "db_props": {
#         "drivername": "postgresql",
#         "host": "",
#         "port": "5432",
#         "username": "postgres",
#         "database": "",
#         "password": ""
#     }
# }
def open_db_props(prop_name, filename='db.properties'):
    props = get_secret(prop_name, filename)
    return db_urlify(props)


def get_secret(setting, filename):
    with open(filename) as secrets_file:
        secrets = json.load(secrets_file)
    """Get secret setting or fail with ImproperlyConfigured"""
    try:
        return secrets[setting]
    except KeyError:
        print("Set the {} setting".format(setting))
        raise


def db_urlify(props):
    return '{drivername}://{username}:{password}@{host}:{port}/{database}'.format(
        **props)

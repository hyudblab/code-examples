# from declarative_models import Student
# from automap_models import Student
from sql_models import Student
from connect import Connector


def main():
    db_type = 'pg'
    connector = Connector('db_scsc', 'pg')
    Student.connect(connector.conn)

    # get all students
    students = Student.get_all_students()
    for student in students:
        print("name", student.name)

    # get eun and update gpa
    # get student
    eun = Student.get_student_by_name('원은영')
    print("학생 선택됨:", eun.name, eun.gpa)

    # update gpa
    eun.update_gpa(2.0)
    print("성적 업데이트 됨", eun.name, eun.gpa)

    connector.close()


'''
학생 선택됨: 원은영 4.1
성적 업데이트 됨 원은영 2.0
'''

if __name__ == "__main__":
    main()

from psycopg2.extras import DictCursor
import traceback


class Student:
    conn = None

    @classmethod
    def connect(cls, connection):
        cls.conn = connection

    def __init__(self, sid, name, login, age, gpa, firstyear):
        self.sid = sid
        self.name = name
        self.login = login
        self.age = age
        self.gpa = gpa
        self.firstyear = firstyear

    @classmethod
    def get_all_students(cls):
        cur = cls.conn.cursor(cursor_factory=DictCursor)
        # cur = conn.cursor()
        sql = 'SELECT * FROM student'
        cur.execute(sql)
        rows = cur.fetchall()

        students = []
        for row in rows:
            student = Student(**row)
            students.append(student)

        return students

    @classmethod
    def get_student_by_name(cls, name):
        cur = cls.conn.cursor(cursor_factory=DictCursor)
        sql = 'SELECT * FROM student WHERE name = %(name)s'
        params = {
            'name': name
        }
        cur.execute(sql, params)
        rows = cur.fetchall()
        student = Student(**rows[0])
        return student

    @classmethod
    def get_student_by_sid(cls, sid):
        cur = cls.conn.cursor(cursor_factory=DictCursor)
        sql = 'SELECT * FROM student WHERE sid = %(sid)s'
        params = {
            'sid': sid
        }
        cur.execute(sql, params)
        rows = cur.fetchall()
        student = Student(**rows[0])
        return student

    def update_gpa(self, gpa):
        cur = self.conn.cursor(cursor_factory=DictCursor)
        sql = 'UPDATE student SET gpa = %(gpa)s WHERE sid = %(sid)s'
        params = {
            'gpa': gpa,
            'sid': self.sid
        }
        cur.execute(sql, params)
        result = cur.rowcount
        self.conn.commit()
        if result == 1:
            return True
        else:
            return False

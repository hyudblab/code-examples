from sqlalchemy import create_engine
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session

from connect import open_db_props

Base = automap_base()


class Student(Base):
    __tablename__ = 'student'

    @staticmethod
    def get_all_students():
        q = session.query(Student)
        print(q.all())
        return q.all()

    @staticmethod
    def get_student_by_name(name):
        q = session.query(Student).filter(Student.name == name).one()
        return q

    @staticmethod
    def get_student_by_sid(sid):
        q = session.query(Student).filter(Student.sid == sid).one()
        return q

    def update_gpa(self, gpa):
        self.gpa = gpa
        try:
            session.commit()
            return True
        except Exception:
            return False


engine = create_engine(open_db_props('db_scsc'))
Base.prepare(engine, reflect=True)

session = Session(engine)

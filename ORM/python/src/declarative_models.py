from sqlalchemy import Column, Integer, String, Float, Date
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Student(Base):
    __tablename__ = 'student'
    sid = Column(String, primary_key=True)
    name = Column(String)
    login = Column(String)
    age = Column(Integer)
    gpa = Column(Float)
    firstyear = Column(Date)

    @classmethod
    def connect(cls, connection):
        cls.session = connection

    @classmethod
    def get_all_students(cls):
        q = cls.session.query(Student)
        print(q.all())
        return q.all()

    @classmethod
    def get_student_by_name(cls, name):
        q = cls.session.query(Student).filter(Student.name == name).one()
        return q

    @classmethod
    def get_student_by_sid(cls, sid):
        q = cls.session.query(Student).filter(Student.sid == sid).one()
        return q

    def update_gpa(self, gpa):
        self.gpa = gpa
        try:
            self.session.commit()
            return True
        except Exception:
            return False

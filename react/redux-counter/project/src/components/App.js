import React from 'react'

class App extends React.Component {
    render() {
        return (
            <div id="App">
                Hello World
            </div>
        )
    }
}

export default App;
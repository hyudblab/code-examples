"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = require("http");
const fs_1 = require("fs");
function runServer() {
    const hostname = "0.0.0.0";
    const port = 3001;
    const server = http_1.createServer((req, res) => {
        res.setHeader("Content-Type", "text/html");
        fs_1.readFile(__dirname + '/web/index.html', (err, data) => {
            if (err) {
                res.statusCode = 404;
                res.end("Not Found");
            }
            res.statusCode = 200;
            res.end(data, 'utf-8');
        });
    });
    server.listen(port, hostname, () => {
        console.log(`Server running at http://${hostname}:${port}/`);
    });
}
runServer();

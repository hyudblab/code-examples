import Koa from 'koa'
// const serve = require('koa-static')


function main() {
  const app = new Koa()
  // app.use(serve(__dirname + '/web'))

  const port = 3001
  app.listen(port)
  console.log("server starting port: " + port)
}

main()
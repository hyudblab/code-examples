"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_1 = __importDefault(require("koa"));
const koa_static_1 = __importDefault(require("koa-static"));
const app = new koa_1.default();
app.use(koa_static_1.default(__dirname + '/web'));
const port = 3001;
const server = app.listen(port);
console.log("server starting port: " + port);

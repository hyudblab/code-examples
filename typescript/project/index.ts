import { createServer } from 'http'
import { readFile } from 'fs'


function runServer() {
  const hostname = "0.0.0.0"
  const port = 3001
  const server = createServer((req, res) => {
    res.setHeader("Content-Type", "text/html")
    readFile(__dirname + '/web/index.html', (err, data) => {
      if (err) {
        res.statusCode = 404
        res.end("Not Found")
      }
      res.statusCode = 200
      res.end(data, 'utf-8')
    })
  })

  server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
  })
}

runServer()
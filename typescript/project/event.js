"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.greet = void 0;
const events_1 = require("events");
function greet() {
    console.log("hello");
    console.log(process.platform);
}
exports.greet = greet;
class CoffeeEvent {
    constructor() {
        this.eventEmitter = new events_1.EventEmitter();
        this.eventEmitter.on('lunch', (callback) => {
            console.log("점심시간");
            callback(3);
        });
    }
    request(cup = 5) {
        this.eventEmitter.emit("lunch", (num) => { console.log("커피 " + (num + cup) + "잔 주문"); });
    }
}

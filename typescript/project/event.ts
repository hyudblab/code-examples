import { EventEmitter } from 'events'


export function greet() {
  console.log("hello")
  console.log(process.platform)
}

class CoffeeEvent {
  eventEmitter: EventEmitter

  constructor() {
    this.eventEmitter = new EventEmitter()
    this.eventEmitter.on('lunch', (callback: (num: number) => void) => {
      console.log("점심시간")
      callback(3)
    })
  }

  request(cup: number = 5) {
    this.eventEmitter.emit("lunch", (num: number) => { console.log("커피 " + (num + cup) + "잔 주문") })
  }
}

class Node:    
    def __init__(self, data):
        self.data = data        
        self.next_node = None

class LinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0
    
    def add_first(self, input):
        node = Node(input)
        node.next_node = self.head
        self.head = node
        self.size += 1
        if self.head.next_node is None:
            self.tail = self.head
    
    def get_all_list(self):
        if self.head is None:
            return []
        
        l = []
        node = self.head
        while node.next_node is not None:
            l.append(node.data)
            node = node.next_node        
        l.append(node.data)
        return l

print("hello")

def main():
    print(__name__)

    linked_list = LinkedList()
    linked_list.add_first(5)
    linked_list.add_first(66)
    print(linked_list.get_all_list())


if __name__ == "__main__":
    main()
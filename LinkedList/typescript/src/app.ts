class LinkedListNode<T> {
  data: T
  next: LinkedListNode<T> | null
  constructor(data: T) {
    this.data = data
    this.next = null
  }
}

class LinkedList<T> {
  head: LinkedListNode<T> | null
  tail: LinkedListNode<T> | null
  size: number

  constructor() {
    this.head = null
    this.tail = null
    this.size = 0
  }
  addFirst(input: T) {
    let node = new LinkedListNode(input)
    node.next = this.head
    this.head = node
    this.size++
    if (this.head.next === null) {
      this.tail = this.head
    }
  }
  getAllList() {
    if (this.head === null) {
      return []
    }
    let l = []
    let node = this.head
    while (node.next !== null) {
      console.log(node)
      l.push(node.data)
      node = node.next
    }
    l.push(node.data) // last element
    return l
  }
}

// main
(function () {
  const linkedList = new LinkedList<number | string>()
  linkedList.addFirst(5)
  linkedList.addFirst("66")
  const p = linkedList.getAllList()
  console.log(p)
})()
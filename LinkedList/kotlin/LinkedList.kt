class Node<T> (value: T) {
    var value = value
    var next: Node<T>? = null
}

class LinkedList<T> {
    var head: Node<T>?
    var tail: Node<T>?
    var size: Int
    constructor() {
        head = null
        tail = null
        size = 0
    }

    fun addFirst(input: T) {
        val h = this.head
        var node: Node<T> = Node(input)
        node.next = this.head
        head = node
        size++
        if (h == null) tail = node        
    }

    fun getAllList(): ArrayList<T> {
        val h = this.head
        if (h == null) {
            return arrayListOf<T>()
        }
        var l: ArrayList<T> = ArrayList()
        var node: Node<T> = h
        while(node.next != null) {
            l.add(node.value)
            node = node.next!!
        }
        l.add(node.value)
        return l
    }
}

fun main() {
    var linkedList: LinkedList<Int> = LinkedList()
    linkedList.addFirst(5)
    linkedList.addFirst(6)
    val p = linkedList.getAllList()
    println(p)
}

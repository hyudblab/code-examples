import java.util.ArrayList;

class Node<T> {
    public T data;
    public Node<T> next;

    public Node(T input){
        data = input;
        next = null;
    }
}

public class LinkedList<T> {
    public Node<T> head;
    public Node<T> tail;
    public int size;

    public LinkedList() {
        head = null;
        tail = null;
        size = 0;
    }

    public void addFirst(T input) {
        Node<T> node = new Node<T>(input);
        node.next = head;
        head = node;
        size++;
        if (head == null) tail = node;
    }

    public ArrayList<T> getAllList() {
        if (head == null) {
            return new ArrayList<T>();
        }
        ArrayList<T> l = new ArrayList<T>();
        Node<T> node = head;
        while(node.next != null) {
            l.add(node.data);
            node = node.next;
        }
        l.add(node.data);
        return l;
    }

    public static void main(String[] args) {
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        linkedList.addFirst(5);  // in old java: addFirst(new Integer(5))
        linkedList.addFirst(66);
        System.out.println(linkedList.getAllList().toString());
    }
}
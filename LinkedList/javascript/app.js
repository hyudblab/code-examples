class Node {
  constructor(data) {
    this.data = data
    this.next = null
  }
  hasNext() {
    if (this.next !== null) return true
    else return false
  }
  getNext() {
    return this.next
  }
}

class LinkedList {
  constructor() {
    this.head = null
    this.tail = null
    this.size = 0
  }
  addFirst(input) {
    let node = new Node(input)
    node.next = this.head
    this.head = node
    this.size++
    if (!this.head.hasNext()) {
      this.tail = this.head
    }
  }
  getAllList() {
    if (this.head === null) {
      return []
    }
    let l = []
    let node = this.head
    while (node.hasNext()) {
      console.log(node)
      l.push(node.data)
      node = node.getNext()
    }
    l.push(node.data) // last element
    return l
  }
}

// main
(function () {
  const linkedList = new LinkedList()
  linkedList.addFirst(5)
  linkedList.addFirst("66")
  const p = linkedList.getAllList()
  console.log(p)
})()


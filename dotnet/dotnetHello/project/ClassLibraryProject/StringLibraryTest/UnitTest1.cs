using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UtilityLibraries;

namespace StringLibraryTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestStartsWithUpper()
        {
            string[] words = { "Alphabet", "Zebra", "ABC", "Αθήνα", "Москва" };
            foreach(var word in words) {
                bool result = word.StartsWithUpper();                

            }
            string someString = "나는 스트링";
            someString.StartsWithUpper();
        }
    }
}

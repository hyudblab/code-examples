﻿using System;
using UtilityLibraries;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello World!");

        int row = 0;

        do
        {
            if (row == 0 || row >= 25) ResetConsole();

            string input = Console.ReadLine();
            if (String.IsNullOrEmpty(input)) break;
              Console.WriteLine($"Input: {input} {"Begins with uppercase? ",50}: " +
                              $"{(input.StartsWithUpper() ? "Yes" : "No")}\n");
        } while (true);
        return;

        void ResetConsole()
        {
            if (row > 0)
            {
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
            Console.Clear();
            Console.WriteLine("\nPress <Enter> only to exit; otherwise, enter a string and press <Enter>:\n");
            row = 3;
        }
    }
}


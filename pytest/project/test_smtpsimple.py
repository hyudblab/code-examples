import pytest


@pytest.fixture(
    scope="module",
    params=[
        {"data": {"server": "https://dbsrv.hanyang.ac.kr", "app_port": 5055}, "expected": True},
        {"data": {"server": "https://dbsrv.hanyang.ac.kr", "app_port": 5056}, "expected": False},
    ],
)
def traccar_app_port_open(request):
    connection_info = request.param["data"]
    import requests
    from urllib.parse import urlparse

    server = urlparse(connection_info["server"])
    address = server.netloc
    port = connection_info["app_port"]

    url = "https://www.traccar.org/port-check/"
    data = {"address": address, "port": port}
    headers = {"Content-Type": "application/x-www-form-urlencoded"}

    res = requests.post(url, data=data, headers=headers)

    yield ("Port is open" in res.text) == request.param["expected"]


def traccar_session_login():
    pass


def test_traccar_status(traccar_app_port_open):
    assert traccar_app_port_open

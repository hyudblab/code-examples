import json


def get_secret(setting, secrets):
    """Get secret setting or fail with ImproperlyConfigured"""
    try:
        return secrets[setting]
    except KeyError:
        print("Set the {} setting".format(setting))
        raise


def open_config(name, filename="db.properties"):
    with open(filename) as secrets_file:
        secrets = json.load(secrets_file)
        return get_secret(name, secrets)

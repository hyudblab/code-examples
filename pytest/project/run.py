from config_manager import open_config


def get_config():
    traccar = open_config("aws-traccar-user")
    print(traccar)
    pass


def smtp_connection():
    import smtplib

    return smtplib.SMTP("smtp.gmail.com", 587, timeout=5)


def traccar_connection():
    import requests
    from urllib.parse import urlparse

    traccar_info = {
        "server": "https://dbsrv.hanyang.ac.kr",
        "baseUrl": "/gps",
        "app_port": 5055,
        "user": {"email": "test", "password": "testman"},
    }
    server = urlparse(traccar_info["server"])
    address = server.netloc
    port = traccar_info["app_port"]

    url = "https://www.traccar.org/port-check/"
    data = {"address": address, "port": port}
    headers = {"Content-Type": "application/x-www-form-urlencoded"}

    res = requests.post(url, data=data, headers=headers)
    from bs4 import BeautifulSoup as bs

    soup = bs(res.text)  # make BeautifulSoup
    prettyHTML = soup.prettify()
    f = open("port_open.html", "w")
    f.write(prettyHTML)
    f.close()

    return res.text


def main():
    # smtp = smtp_connection()
    # response, msg = smtp.ehlo()
    # print(smtp)
    # print(response, msg)
    # result = traccar_connection()
    # print(result)
    print(get_config())


if __name__ == "__main__":
    main()

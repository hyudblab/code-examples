"use strict";
exports.__esModule = true;
var Koa = require("koa");
var serve = require("koa-static");
function main() {
    var app = new Koa();
    app.use(serve("./dist"));
    var port = 80;
    var server = app.listen(port);
    console.log("server start port: " + port);
}
main();

function main() {
  const root = document.getElementById("root")

  Notification.requestPermission().then(result => {
    console.log(result)
  })

  getTime(root)
}

function getTime(root) {
  setInterval((root) => {
    const date = new Date()
    const currentTime = root.querySelector("#timer")
    currentTime.innerHTML = date

    const alarm: FormData = root.querySelector("#alarm")

    const hour: string = alarm["hour"].value
    const minute: string = alarm["minute"].value

    const currHour = date.getHours()
    const currMinute = date.getMinutes()

    if (date.getSeconds() === 0) {
      if (currMinute === parseInt(minute)) {
        if (hour.length === 0 || parseInt(hour) === currHour) {
          const noti = new Notification("Get up!")
          setTimeout(noti.close.bind(noti), 4000)
        }
      }
    }
  }, 1000, root)



}

main()
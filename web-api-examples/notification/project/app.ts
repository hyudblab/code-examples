import * as Koa from 'koa'
import * as serve from 'koa-static'

function main() {
  const app = new Koa()
  app.use(serve("./dist"))

  const port = 80
  const server = app.listen(port)

  console.log("server start port: " + port)
}

main()